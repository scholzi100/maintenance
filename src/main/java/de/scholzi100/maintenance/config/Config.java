/*
 * Copyright (c) 2015 Paul Scholz (scholzi100) All rights reserved.
 */

package de.scholzi100.maintenance.config;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 * Created by scholzi100 on 16.02.15.
 */

@Data
public class Config {

    @SerializedName("Kick Nachricht")
    private String kickMessage;

    @SerializedName("Eingeschaltet")
    private boolean enabled;
    
}
