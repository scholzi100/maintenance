/*
 * Copyright (c) 2015 Paul Scholz (scholzi100) All rights reserved.
 */

package de.scholzi100.maintenance.commands;

import de.scholzi100.maintenance.MaintenancePlugin;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

import java.util.Arrays;

/**
 * Created by scholzi100 on 16.02.15.
 */
public class MaintenanceCommand extends Command {

    private final MaintenancePlugin plugin;
    

    public MaintenanceCommand(MaintenancePlugin plugin) {
        super("maintenance","network.maintenance", "mat");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 1){
            String subcommand = args[0];
            if (subcommand.equalsIgnoreCase("status")) {
                sender.sendMessage(plugin.getPrefix() + (plugin.getConfig().isEnabled() ? "Der Wartungsmodus ist an!" : "Der Wartungsmodus ist aus!"));
            }else if (subcommand.equalsIgnoreCase("enable")){
                if (plugin.getConfig().isEnabled()){
                    sender.sendMessage(plugin.getPrefix()+"Der Wartungsmodus ist noch an!");
                    return;
                }
                plugin.getConfig().setEnabled(true);
                plugin.saveConfig();
                sender.sendMessage(plugin.getPrefix()+"Der Wartungsmodus ist nun an!");
            }else if (subcommand.equalsIgnoreCase("disable")){
                if (!plugin.getConfig().isEnabled()){
                    sender.sendMessage(plugin.getPrefix()+"Der Wartungsmodus ist schon aus!");
                    return;
                }
                plugin.getConfig().setEnabled(false);
                try {
                    plugin.saveConfig();
                } catch (Exception e) {
                    sender.sendMessage(plugin.getPrefix() + "Es gab ein Problem beim speichern der Config!");
                    e.printStackTrace();
                    return;
                }
                sender.sendMessage(plugin.getPrefix()+"Der Wartungsmodus ist nun aus!");
            }else if (subcommand.equalsIgnoreCase("reload")){
                try {
                    plugin.loadConfig();
                } catch (Exception e) {
                    sender.sendMessage(plugin.getPrefix()+"Es gab ein Problem beim neuladen der Config!");
                    e.printStackTrace();
                    return;
                }
                sender.sendMessage(plugin.getPrefix()+"Die Config wurde neugeladen!");
            }else sendUsage(sender);
        }else sendUsage(sender);
    }
    
    private void sendUsage(CommandSender sender){
        sender.sendMessage(plugin.getPrefix() + "Benutze bitte \"/maintenance status|enable|disable|reload\"!");
    }
}
