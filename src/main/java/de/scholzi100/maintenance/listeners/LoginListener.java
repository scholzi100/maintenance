/*
 * Copyright (c) 2015 Paul Scholz (scholzi100) All rights reserved.
 */

package de.scholzi100.maintenance.listeners;

import de.scholzi100.maintenance.MaintenancePlugin;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

/**
 * Created by scholzi100 on 16.02.15.
 */
public class LoginListener implements Listener {

    private final MaintenancePlugin plugin;

    public LoginListener(MaintenancePlugin plugin) {
        this.plugin = plugin;
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onLogin(ServerConnectEvent event){
        if (plugin.getConfig().isEnabled()&&!event.getPlayer().hasPermission("network.maintenance")){
            event.getPlayer().disconnect(plugin.getConfig().getKickMessage().replace("&","§").replace("\\n","\n"));
            event.setCancelled(true);
            return;
        }
    }
    
}
