/*
 * Copyright (c) 2015 Paul Scholz (scholzi100) All rights reserved.
 */

package de.scholzi100.maintenance;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import de.scholzi100.maintenance.commands.MaintenanceCommand;
import de.scholzi100.maintenance.config.Config;
import de.scholzi100.maintenance.listeners.LoginListener;
import lombok.Getter;
import net.md_5.bungee.api.plugin.Plugin;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by scholzi100 on 16.02.15.
 */
public class MaintenancePlugin extends Plugin{

    private File folder;
    private File file;
    private Gson gson;
    @Getter
    private Config config;
    @Getter
    private String prefix;

    @Override
    public void onDisable() {
        folder = null;
        file = null;
        gson = null;
        config = null;
    }

    @Override
    public void onEnable() {
        folder = new File("plugins"+File.separator+"Maintenance"+File.separator);
        file = new File(folder,"config.json");
        gson = new GsonBuilder().setPrettyPrinting().create();
        loadConfig();
        prefix = "§7[§cMaintenance§7]§f ";
        getProxy().getPluginManager().registerListener(this,new LoginListener(this));
        getProxy().getPluginManager().registerCommand(this,new MaintenanceCommand(this));
    }
    
    public void loadConfig(){
        if (!folder.exists()) {
            folder.mkdir();
            folder.mkdirs();
        }
        if (!file.exists()) {

            saveConfig();
            
            FileReader reader;
            try {
                reader = new FileReader(file);
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
            try {
                Config config = gson.fromJson(reader, Config.class);
                this.config = config;
            } catch (JsonSyntaxException e) {
                throw new RuntimeException(e);
            } catch (JsonIOException e) {
                throw new RuntimeException(e);
            } finally {
                try {
                    reader.close();
                } catch (Exception e) {
                }
            }

        } else {
            FileReader reader;
            try {
                reader = new FileReader(file);
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
            try {
                Config config = gson.fromJson(reader, Config.class);
                this.config = config;
            } catch (JsonSyntaxException e) {
                throw new RuntimeException(e);
            } catch (JsonIOException e) {
                throw new RuntimeException(e);
            } finally {
                try {
                    reader.close();
                } catch (Exception e) {
                }
            }
        }
    }
    
    public void saveConfig(){
        if (!folder.exists()) {
            folder.mkdir();
            folder.mkdirs();
        }
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            URL resource = getClass().getClassLoader().getResource("config.json");

            URLConnection connection;
            String raw = null;
            StringWriter stWriter = null;
            try {
                connection = resource.openConnection();
                stWriter = new StringWriter();
                IOUtils.copy(connection.getInputStream(), stWriter);
                raw = stWriter.toString();

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    stWriter.close();
                } catch (IOException e) {
                }
            }

            if (raw == null) throw new RuntimeException("config.json is null");

            FileWriter writer;
            try {
                writer = new FileWriter(file);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            try {
                writer.write(raw);
            } catch (IOException e) {
                throw new RuntimeException(e);
            } finally {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            FileWriter writer;
            try {
                writer = new FileWriter(file);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            try {
                gson.toJson(this.config,writer);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }finally {
                try {
                    writer.close();
                } catch (Exception e) {
                }
            }
        }
    }
    
}
